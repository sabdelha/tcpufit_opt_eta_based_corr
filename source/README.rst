====================================================
Template MET Trigger Performance Code for applying eta_based corrections on the cluster containers
====================================================


Setup
-----

Prepare the directory structure and clone the git repository::

    mkdir build run
    git clone https://gitlab.cern.ch/sabdelha/tcpufit_opt_eta_based_corr.git source

Setup the release and compile the code. For basic performance studies ``latest`` is sufficient but if doing longer-term work it's best to pick a stable release::

    cd build
    setupATLAS
    asetup Athena master latest
    cmake ../source
    make
    . ${Athena_PLATFORM}/setup.sh

On future sessions you will only need to set up the release and source the setup file.

Running the code
----------------

There is now only one running script ``run.py``.
This uses the metadata to work out if it is running on MC or data.

These should be run as::
    
    python -m MasterPerf.run /path/to/file1 /path/to/file2 etc

It is not able to work out if it is running over EB data, so in this case you must the flag ``--is-eb``.
To see all the available options run::

    python -m MasterPerf.run --help

Modifying the code
------------------
I did add an algorithm to copy the cluster containers and scale its pt which then used by the tcpufit_Fex algorithm to calculate MET. Also, I had to add this new algorithm in the job sequence. 
