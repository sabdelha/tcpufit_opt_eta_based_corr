#include "NTupleAlg.h"
#include "StoreGate/ReadHandle.h"

NTupleAlg::NTupleAlg(const std::string &name, ISvcLocator *pSvcLocator) : AthAnalysisAlgorithm(name, pSvcLocator)
{
  declareProperty("TrigDecisionTool", m_tdt);
}

NTupleAlg::~NTupleAlg() {}

StatusCode NTupleAlg::initialize()
{
  ATH_CHECK(m_triggerMETs.initialize());
  ATH_CHECK(m_tdt.retrieve());
  if (m_isEB)
    ATH_CHECK(m_ebTool.retrieve());
  if (m_isMC)
    ATH_CHECK(m_truthMET.initialize(SG::AllowEmpty));
  ATH_CHECK(m_evtInfo.initialize());
  ATH_CHECK(m_l1MET.initialize());
  ATH_CHECK(m_recoMET.initialize(SG::AllowEmpty));
  ATH_CHECK(m_muons.initialize(SG::AllowEmpty));
  const char *treeName = m_treeName.value().c_str();
  m_tree = bookGetPointer(TTree(treeName, treeName));
  if (!m_tree)
    return StatusCode::FAILURE;
  m_trigMet.reserve(m_triggerMETs.size());
  m_trigPhi.reserve(m_triggerMETs.size());
  m_trigSumEt.reserve(m_triggerMETs.size());
  for (const SG::ReadHandleKey<xAOD::TrigMissingETContainer> &key : m_triggerMETs)
  {
    std::string name = key.key();
    if (name.substr(0, 8) == "HLT_MET_")
      name = name.substr(8);
    m_trigMet.push_back(0.);
    m_tree->Branch((name + ".met").c_str(), &m_trigMet.back());
    m_trigPhi.push_back(0.);
    m_tree->Branch((name + ".phi").c_str(), &m_trigPhi.back());
    m_trigSumEt.push_back(0.);
    m_tree->Branch((name + ".sumet").c_str(), &m_trigSumEt.back());
  }
  m_trigDecisions.reserve(m_triggerNames.size());
  m_trigPrescales.reserve(m_trigPrescales.size());
  for (const std::string &trigger : m_triggerNames)
  {
    m_trigDecisions.push_back(false);
    m_tree->Branch((trigger + ".isPassed").c_str(), &m_trigDecisions.back());
    m_trigPrescales.push_back(1.0);
    m_tree->Branch((trigger + ".prescale").c_str(), &m_trigPrescales.back());
  }
  if (m_isEB)
  {
    m_tree->Branch("EBWeight", &m_ebWeight);
    m_tree->Branch("EBLiveTime", &m_ebLiveTime);
    m_tree->Branch("LBLumi", &m_lbLumi);
    m_tree->Branch("IsGoodLB", &m_isGoodLB);
    m_tree->Branch("IsUnbiasedEvent", &m_isUnbiasedEvent);
  }
  if (m_isMC)
  {
    m_tree->Branch("MCEventWeight", &m_mcEventWeight);
    if (!m_truthMET.empty())
      for (std::string term : {"NonInt", "Int", "IntMuons", "NonIntPlusIntMuons", "NonIntMinusIntMuons"})
      {
        m_tree->Branch(("Truth." + term + ".mpx").c_str(), &m_truthMpx[term]);
        m_tree->Branch(("Truth." + term + ".mpy").c_str(), &m_truthMpy[term]);
        m_tree->Branch(("Truth." + term + ".met").c_str(), &m_truthMet[term]);
        m_tree->Branch(("Truth." + term + ".sumet").c_str(), &m_truthSumEt[term]);
      }
  }
  m_tree->Branch("AverageInteractionsPerCrossing", &m_averageInteractionsPerCrossing);
  m_tree->Branch("ActualInteractionsPerCrossing", &m_actualInteractionsPerCrossing);
  m_tree->Branch("L1.met", &m_l1Met);
  m_tree->Branch("L1.phi", &m_l1Phi);
  m_tree->Branch("L1.sumet", &m_l1SumEt);
  if (!m_recoMET.empty())
  {
    m_tree->Branch("Reco.met", &m_recoMet);
    m_tree->Branch("Reco.phi", &m_recoPhi);
    m_tree->Branch("Reco.sumet", &m_recoSumEt);
    m_tree->Branch("RecoNoMu.met", &m_recoNoMuMet);
    m_tree->Branch("RecoNoMu.phi", &m_recoNoMuPhi);
    m_tree->Branch("RecoNoMu.sumet", &m_recoNoMuSumEt);
  }
  if (!m_muons.empty())
  {
      m_tree->Branch("Muons.pt", &m_muonPt);
      m_tree->Branch("Muons.eta", &m_muonEta);
      m_tree->Branch("Muons.phi", &m_muonPhi);
      m_tree->Branch("Dimuon.mass", &m_diMuonMass);
      m_tree->Branch("Dimuon.pt", &m_diMuonPt);
      m_tree->Branch("Dimuon.eta", &m_diMuonEta);
      m_tree->Branch("Dimuon.phi", &m_diMuonPhi);
  }
  return StatusCode::SUCCESS;
}

StatusCode NTupleAlg::execute()
{
  m_trigMet.clear();
  m_trigPhi.clear();
  m_trigSumEt.clear();
  m_trigDecisions.clear();
  m_trigPrescales.clear();
  m_muonPt.clear();
  m_muonEta.clear();
  m_muonPhi.clear();
  m_diMuonMass = -999;
  m_diMuonPt = -999;
  m_diMuonEta = -999;
  m_diMuonPhi = -999;

  for (const SG::ReadHandleKey<xAOD::TrigMissingETContainer> &key : m_triggerMETs)
  {
    SG::ReadHandle<xAOD::TrigMissingETContainer> met = SG::makeHandle(key);
    if (met->size() == 0)
    {
      m_trigMet.push_back(-1);
      m_trigPhi.push_back(-1);
      m_trigSumEt.push_back(-1);
    }
    else
    {
      const xAOD::TrigMissingET &metObj = *(met->at(0));
      float mpx = metObj.ex();
      float mpy = metObj.ey();
      m_trigMet.push_back(sqrt(mpx * mpx + mpy * mpy));
      m_trigPhi.push_back(atan2(mpy, mpx));
      m_trigSumEt.push_back(metObj.sumEt());
    }
  }

  for (const std::string &trig : m_triggerNames)
  {
    m_trigDecisions.push_back(m_tdt->isPassed(trig));
    m_trigPrescales.push_back(m_tdt->getChainGroup(trig)->getPrescale());
  }

  SG::ReadHandle<xAOD::EventInfo> evtInfo = SG::makeHandle(m_evtInfo);
  if (m_isEB)
  {
    m_ebWeight = m_ebTool->getEBWeight(evtInfo.ptr());
    m_ebLiveTime = m_ebTool->getEBLiveTime(evtInfo.ptr());
    m_lbLumi = m_ebTool->getLBLumi(evtInfo.ptr());
    m_isGoodLB = m_ebTool->isGoodLB(evtInfo.ptr());
    m_isUnbiasedEvent = m_ebTool->isUnbiasedEvent(evtInfo.ptr());
  }
  if (m_isMC)
  {
    m_mcEventWeight = evtInfo->mcEventWeight(0);
    if (!m_truthMET.empty())
    {
      SG::ReadHandle<xAOD::MissingETContainer> met = SG::makeHandle(m_truthMET);
      const xAOD::MissingET &nonIntMET = *((*met)["NonInt"]);
      const xAOD::MissingET &muonsMET = *((*met)["IntMuons"]);
      const xAOD::MissingET &intMET = *((*met)["Int"]);
      fillMET("Int", intMET);
      fillMET("NonInt", nonIntMET);
      fillMET("IntMuons", muonsMET);
      fillMET("NonIntPlusIntMuons", nonIntMET + muonsMET);
      fillMET("NonIntMinusIntMuons", nonIntMET - muonsMET);
    }
  }
  m_actualInteractionsPerCrossing = evtInfo->actualInteractionsPerCrossing();
  m_averageInteractionsPerCrossing = evtInfo->averageInteractionsPerCrossing();

  if (!m_recoMET.empty())
  {
    SG::ReadHandle<xAOD::MissingETContainer> offlineMET = SG::makeHandle(m_recoMET);
    //for (const xAOD::MissingET* x : *offlineMET)
    //ATH_MSG_INFO(x->name());
    const xAOD::MissingET *finalTrkMET = ((*offlineMET)["FinalTrk"]);
    const xAOD::MissingET *muonsMET = ((*offlineMET)["Muons"]);
    ATH_MSG_DEBUG("Retrieved MET components " << finalTrkMET <<", " << muonsMET);
    if (!finalTrkMET || !muonsMET)
    {
      ATH_MSG_ERROR("Failed to retrieve MET components!!");
      return StatusCode::FAILURE;
    }
    xAOD::MissingET finalTrkNoMuMET = *finalTrkMET - *muonsMET;
    m_recoMet = finalTrkMET->met();
    m_recoPhi = finalTrkMET->phi();
    m_recoSumEt = finalTrkMET->sumet();
    m_recoNoMuMet = finalTrkNoMuMET.met();
    m_recoNoMuPhi = finalTrkNoMuMET.phi();
    m_recoNoMuSumEt = finalTrkNoMuMET.sumet();
  }

  SG::ReadHandle<xAOD::EnergySumRoI> l1 = SG::makeHandle(m_l1MET);
  float mpx = l1->energyX();
  float mpy = l1->energyY();
  m_l1Met = sqrt(mpx * mpx + mpy * mpy);
  m_l1Phi = atan2(mpy, mpx);
  m_l1SumEt = l1->energyT();

  if (!m_muons.empty())
  {
      auto muons = SG::makeHandle(m_muons);
      if (!muons.isValid())
      {
          ATH_MSG_ERROR("Failed to retrieve " << m_muons);
          return StatusCode::FAILURE;
      }
      for (const xAOD::Muon *imu : *muons)
      {
          m_muonPt.push_back(imu->pt());
          m_muonEta.push_back(imu->eta());
          m_muonPhi.push_back(imu->phi());
      }
      if (muons->size() == 2)
      {
          TLorentzVector p4 = muons->at(0)->p4() + muons->at(1)->p4();
          m_diMuonMass = p4.M();
          m_diMuonPt = p4.Pt();
          m_diMuonEta = p4.Eta();
          m_diMuonPhi = p4.Phi();
      }
  }


  m_tree->Fill();
  return StatusCode::SUCCESS;
}

void NTupleAlg::fillMET(const std::string &term, const xAOD::MissingET &met)
{
  m_truthMpx[term] = met.mpx();
  m_truthMpy[term] = met.mpy();
  m_truthMet[term] = met.met();
  m_truthSumEt[term] = met.sumet();
}

DECLARE_COMPONENT(NTupleAlg)
