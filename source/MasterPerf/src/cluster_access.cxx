#include <xAODCaloEvent/CaloClusterContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <src/cluster_access.h> 
#include <AsgMessaging/MessageCheck.h>
#include "GaudiKernel/SystemOfUnits.h"
#include <StoreGate/ReadHandleKey.h>

cluster_access :: cluster_access (const std::string &name, ISvcLocator *pSvcLocator) : AthReentrantAlgorithm(name, pSvcLocator)
    
{
}

cluster_access::~cluster_access() {}

StatusCode cluster_access :: initialize ()

{

CHECK(m_clusterKey.initialize());

return StatusCode::SUCCESS;
}

StatusCode cluster_access :: execute (const EventContext& context) const
{

auto clusters = SG::makeHandle(m_clusterKey,context);
  if (!clusters.isValid())
  {
    ATH_MSG_ERROR("Failed to retrieve " << m_clusterKey);
    return StatusCode::FAILURE;
}


auto cls = xAOD::CaloClusterContainer(*clusters);
//const xAOD::CaloClusterContainer* cls = nullptr;

//auto newcont = std::unique_ptr<xAOD::CaloClusterContainer>(cls.first);

float_t cluster_energy[5001]={0};
//float_t cluster_eta[5001]={0};
int j=0;

for (auto  cls : *(clusters))
{   
    //for (float_t i=0;i<5;i+=0.001){
      if (-5<cls->eta() && cls->eta()<5){
         std::cout << "clustereta " << cls->eta() << std::endl;
 //if (cls->eta()==i){
         //cluster_eta[j]=cls->eta();
       cluster_energy[j]=cls->e()*0.001;
 //     std::cout << "cluster energyj " << cluster_energy[j] << std::endl;
   //   std::cout << "cluster etaj " << cluster_eta[j] << std::endl;
      
 //}
    // }


//ATH_MSG_INFO ("execute(): cluster energy1 = " << cluster_energy[1] << " GeV");
//ATH_MSG_INFO ("execute(): cluster eta1 = " << cluster_eta[1] << " degrees");

//ATH_MSG_INFO ("execute(): cluster energy2000 = " << cluster_energy[2000] * 0.001 << " GeV");
//ATH_MSG_INFO ("execute(): cluster eta2000 = " << cluster_eta[2000] << " degrees");

    //j=j+1;
}
j=j+1;
}
std::cout << "cluster energy2 " << cluster_energy[2] << std::endl;

return StatusCode::SUCCESS;
}

//std::cout << "cluster energy5 " << cluster_energy[5] << std::endl;
//td::cout << "cluster eta5 " << cluster_eta[5] << std::endl;

DECLARE_COMPONENT(cluster_access)




//ATH_MSG_INFO ("in execute");

// retrieve the eventInfo object from the event store

//const xAOD::EventInfo *eventInfo = nullptr;

//ATH_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

// print out run and event number from retrieved object

//ATH_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

// get calocluster container that we want:

 //const xAOD::CaloClusterContainer* cls = nullptr;
  //ATH_CHECK (evtStore()->retrieve (cls, "HLT_TopoCaloClustersLCFS"));

// loop over the clsuters in the container and print its pt

//for (const xAOD::CaloCluster* cl : *cls) {
//    ATH_MSG_INFO ("execute(): cluster et = " << (cl->e() * 0.001) << " GeV"); 
 // } // end for loop over jets













//return StatusCode::SUCCESS;
//}









//StatusCode cluster_access :: finalize ()
//{








//return StatusCode::SUCCESS;
//}






