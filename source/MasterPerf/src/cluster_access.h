#ifndef MsterPerf_CLUSTER_ACCESS_H
#define MasterPerf_CLUSTER_ACCESS_H
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "StoreGate/ReadHandleKey.h"
#include "AsgTools/ToolHandle.h"
#include <string>
#include <vector>
#include <memory>
#include <algorithm>
#include <iterator>
#include "xAODCaloEvent/CaloClusterContainer.h"

class cluster_access : public AthReentrantAlgorithm
{
   public:
      cluster_access (const std::string& name, ISvcLocator* pSvcLocator);
      ~cluster_access();
     
      virtual StatusCode initialize() ;
      virtual StatusCode execute(const EventContext& context) const override;



   private:
   
SG::ReadHandleKey<xAOD::CaloClusterContainer> m_clusterKey{
     this, "InputCluster", "clusters", "Input cluster collection"};


};
#endif









