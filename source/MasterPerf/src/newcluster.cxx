#include <AsgMessaging/MessageCheck.h>
#include <src/newcluster.h>
#include "StoreGate/ReadHandle.h"
#include <xAODCore/AuxContainerBase.h>
#include <xAODJet/JetContainer.h>
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowAuxInfo.h"
#include "xAODCore/ShallowCopy.h"
#include <StoreGate/ReadHandleKey.h>

newcluster :: newcluster (const std::string &name, ISvcLocator *pSvcLocator) : AthReentrantAlgorithm(name, pSvcLocator)
    
{
}

newcluster::~newcluster() {}

StatusCode newcluster :: initialize ()
{
CHECK(m_clusterKey.initialize());
CHECK(m_writeHandle.initialize());
//m_systematicsList.addHandle (m_writeHandle);
return StatusCode::SUCCESS;
}


StatusCode newcluster :: execute (const EventContext& context) const 
{

auto clusters = SG::makeHandle(m_clusterKey,context);
  if (!clusters.isValid())
  {
    ATH_MSG_ERROR("Failed to retrieve " << m_clusterKey);
    return StatusCode::FAILURE;
}
auto newClusterContainers = xAOD::shallowCopyContainer(*clusters);
//auto newauxClusterContainers = xAOD::ShallowAuxContainer(*clusters);

auto newcont = std::unique_ptr<xAOD::CaloClusterContainer>(newClusterContainers.first);
auto newcontAux = std::unique_ptr<xAOD::ShallowAuxContainer>(newClusterContainers.second);

newcont->setStore (newcontAux.get());


for (auto  iclus : *(newClusterContainers.first))
{
      float_t newpt=0;
      float_t eta= abs(iclus->eta());
   if (0<eta && eta <0.6) {
      newpt = iclus->e()*(1/0.9);
}
   if (0.6<eta && eta <1.4){
    newpt = iclus->e()*(1/0.795);
}
   if (1.4<eta && eta <1.8)  {
  newpt = iclus->e()*(1/0.745);
}
   if (1.8<eta)  {
  newpt = iclus->e()*(1/0.735);
}
    iclus->setE(newpt);

}
auto outputClusters = SG::makeHandle(m_writeHandle,context);

CHECK(outputClusters.record(std::move(newcont), std::move(newcontAux)));





return StatusCode::SUCCESS;
}



DECLARE_COMPONENT(newcluster)



//StatusCo
//{




//return StatusCode::SUCCESS;
//}



