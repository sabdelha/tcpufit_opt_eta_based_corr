#ifndef MasterPerf_NTupleAlg_H
#define MasterPerf_NTupleAlg_H

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "StoreGate/ReadHandleKey.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/IAsgTool.h"
#include "EnhancedBiasWeighter/IEnhancedBiasWeighter.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "Gaudi/Property.h"

class NTupleAlg : public AthAnalysisAlgorithm
{
public:
  NTupleAlg(const std::string &name, ISvcLocator *pSvcLocator);
  ~NTupleAlg();

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;

private:
  // Inputs
  SG::ReadHandleKeyArray<xAOD::TrigMissingETContainer> m_triggerMETs{
      this, "TriggerMETs", {}, "The trigger MET values to write"};
  SG::ReadHandleKey<xAOD::EnergySumRoI> m_l1MET{
      this, "L1MET", "LVL1EnergySumRoI", "The L1 object"};
  SG::ReadHandleKey<xAOD::MissingETContainer> m_truthMET{
      this, "TruthMET", "", "The truth MET object"};
  Gaudi::Property<std::vector<std::string>> m_triggerNames{
      this, "Triggers", {}, "The names of triggers to write"};
  SG::ReadHandleKey<xAOD::EventInfo> m_evtInfo{
      this, "EventInfo", "EventInfo", "The event info to use"};
  // Output
  Gaudi::Property<std::string> m_treeName{
      this, "TreeName", "METTree", "The name of the output tree"};
  Gaudi::Property<bool> m_isEB{
      this, "IsEB", false, "Whether this is an EB dataset"};
  Gaudi::Property<bool> m_isMC{
      this, "IsMC", false, "Whether this is an MC dataset"};
  ToolHandle<Trig::TrigDecisionTool> m_tdt;
  ToolHandle<IEnhancedBiasWeighter> m_ebTool{
      this, "EBWeightsTool", "", "The EB weights tool"};
  SG::ReadHandleKey<xAOD::MissingETContainer> m_recoMET{
      this, "RecoMET", "MET_Reference_AntiKt4EMPFlow", "The PFlow reference MET"};
  SG::ReadHandleKey<xAOD::MuonContainer> m_muons{
      this, "Muons", "", "The muon container"};

  std::vector<float> m_trigMet;
  std::vector<float> m_trigPhi;
  std::vector<float> m_trigSumEt;
  std::vector<char> m_trigDecisions;
  std::vector<float> m_trigPrescales;
  float m_ebWeight{0.};
  float m_ebLiveTime{0.};
  float m_lbLumi{0.};
  bool m_isGoodLB{true};
  bool m_isUnbiasedEvent{true};
  float m_mcEventWeight{0.};
  float m_averageInteractionsPerCrossing;
  float m_actualInteractionsPerCrossing;
  std::map<std::string, float> m_truthMpx;
  std::map<std::string, float> m_truthMpy;
  std::map<std::string, float> m_truthMet;
  std::map<std::string, float> m_truthSumEt;
  float m_l1Met;
  float m_l1Phi;
  float m_l1SumEt;
  float m_recoPhi;
  float m_recoMet;
  float m_recoSumEt;
  float m_recoNoMuPhi;
  float m_recoNoMuMet;
  float m_recoNoMuSumEt;
  std::vector<float> m_muonPt;
  std::vector<float> m_muonEta;
  std::vector<float> m_muonPhi;
  float m_diMuonMass;
  float m_diMuonPt;
  float m_diMuonEta;
  float m_diMuonPhi;

  TTree *m_tree;

  void fillMET(const std::string &term, const xAOD::MissingET &met);

}; //> end class NTupleAlg

#endif //> !MasterPerf_NTupleAlg_H
