#ifndef MasterPerf_NEWCLUSTER_H
#define MasterPerf_NEWCLUSTER_H
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
//#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
//#include "AthenaMonitoringKernel/GenericMonitoringTool.h"
#include "xAODEventInfo/EventInfo.h"
#include "Gaudi/Property.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "GaudiKernel/SystemOfUnits.h"
//#include "MonGroupBuilder.h"
#include "StoreGate/ReadHandleKey.h"
#include "AsgTools/ToolHandle.h"
#include <string>
#include <vector>
#include <xAODCore/ShallowCopy.h>
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "AsgTools/IAsgTool.h"
#include <StoreGate/WriteHandleKey.h>
#include <memory>
#include <algorithm>
#include <iterator>

//#include "FexBase.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"



class newcluster : public AthReentrantAlgorithm
{
   public:
      newcluster (const std::string& name, ISvcLocator* pSvcLocator);
      ~newcluster();
     
      virtual StatusCode initialize() ;
      virtual StatusCode execute(const EventContext& context) const override;



   private:
   
   SG::ReadHandleKey<xAOD::CaloClusterContainer> m_clusterKey{
     this, "InputCluster", "clusters", "Input cluster collection"};


   SG::WriteHandleKey<xAOD::CaloClusterContainer> m_writeHandle{
     this, "ClusterName", "outputClusters", "output cluster collection"};
   


};
#endif



