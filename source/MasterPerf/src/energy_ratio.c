void energy_ratio()
{
Int_t singleFile1 = 0;
TString inDir = "./";
inDir="/Users/ska6969/Documents/eta_based/cluster_study/";
TChain * chain_FS = new TChain("trigMetTree");
TChain * chain_LCFS = new TChain("trigMetTree");
//Tchain * chain_both = new TChain("trigMetTree");


std::cout << "Adding files from " << inDir << std::endl;


chain_FS->Add(inDir+"FS.signal");
TTree * fs_tree = chain_FS;

chain_LCFS->Add(inDir+"LCFS.signal");
TTree * lcfs_tree = chain_LCFS;

std::vector<double>* lcfs_cluster_energy = new std::vector<double>();
std::vector<double>* lcfs_cluster_eta = new std::vector<double>();
    // Set the branch addresses for the cluster energy and eta values for the LCFS clusters
    lcfs_tree->SetBranchAddress("HLT_cluster_e", &lcfs_cluster_energy);
    lcfs_tree->SetBranchAddress("HLT_cluster_eta", &lcfs_cluster_eta);

    std::vector<double>* fs_cluster_energy = new std::vector<double>();
    std::vector<double>* fs_cluster_eta = new std::vector<double>();

    // Set the branch addresses for the cluster energy and eta values for the FS clusters
    fs_tree->SetBranchAddress("HLT_cluster_e", &fs_cluster_energy);
    fs_tree->SetBranchAddress("HLT_cluster_eta", &fs_cluster_eta);


// Define the energy and eta bins
    const int N_energy_bins = 50;
    const double energy_min = 0;
    const double energy_max = 400.0;
    const int N_eta_bins = 20;
    const double eta_min = -5.0;
    const double eta_max = 5.0;


    // Define a 2D vector to store the FS energies for each event
    std::vector<std::vector<double>> fs_energies(N_eta_bins, std::vector<double>(N_energy_bins, 0.0));

    // Define a 2D vector to store the energy ratios for each event
    std::vector<std::vector<double>> energy_ratios(N_eta_bins, std::vector<double>(N_energy_bins, 0.0));
//TH1F* h_cls_e_FS = new TH1F("h_cls_e_FS", "FS clcuter energy", 100,-2,250);




//std::vector<std::vector<double>> lcfs_energies(N_eta_bins, std::vector<double>(N_energy_bins, 0.0));
std::vector<vector<double>> lcfs_energies(N_eta_bins, vector<double>(N_energy_bins, 0.0));  
    // Loop over the events in the LCFS file
    int n_events = lcfs_tree->GetEntries();
    for (int i = 0; i < n_events; i++) {
       
        lcfs_tree->GetEntry(i);


        //h_cls_e_FS->Fill(fs_cluster_energy);
        
        // Define a 2D vector to store the LCFS energies for the current event
//        std::vector<std::vector<double>> lcfs_energies(N_eta_bins, std::vector<double>(N_energy_bins, 0.0));


        // Loop over the LCFS clusters in the current event
        for (int j = 0; j < lcfs_cluster_energy->size(); j++) {
            // Get the cluster energy and eta values for the current LCFS cluster
            double energy = abs((*lcfs_cluster_energy)[j]);
            double eta = (*lcfs_cluster_eta)[j];
       //std::cout << "energy" << energy << std::endl;
    
    
            // Store the cluster energy in the corresponding energy and eta bin
            int eta_bin = int((eta - eta_min)/(eta_max - eta_min)*N_eta_bins);
            int energy_bin = int((energy - energy_min)/(energy_max - energy_min)*N_energy_bins);
       lcfs_energies[eta_bin][energy_bin] = (*lcfs_cluster_energy)[j];
  
//            std::cout << "energy_bin" << energy_bin << std::endl;
 //           std::cout << "eta_bin" << eta_bin << std::endl;
   //          std::cout << "e" << energy << std::endl;
 }
        }
        

        // Loop over the FS clusters in the current event
        for (int j = 0; j < fs_tree->GetEntries(); j++) {
           

            // Get the cluster energy and eta values for the FS clusters in the current event
            fs_tree->GetEntry(j);

            // Loop over the FS clusters in the current event
            for (int k = 0; k < fs_cluster_energy->size(); k++) {
                // Get the cluster energy and eta values for the current FS cluster
                double energy = (*fs_cluster_energy)[k];
                double eta = (*fs_cluster_eta)[k];

                // Store the cluster energy in the corresponding energy and eta bin
                int eta_bin = int((eta - eta_min) / (eta_max - eta_min) * N_eta_bins);
                int energy_bin = int((energy - energy_min) / (energy_max - energy_min) * N_energy_bins);
                fs_energies[eta_bin][energy_bin] = energy;
            }
        }

        // Loop over the LCFS clusters in the current event
        for (int j = 0; j < lcfs_cluster_energy->size(); j++) {
            // Get the cluster energy and eta values for the current LCFS cluster
            double eta = (*lcfs_cluster_eta)[j];
            double energy = (*lcfs_cluster_energy)[j];

            // Calculate the energy ratio for the current LCFS cluster
            double ratio = 0.0;
            int eta_bin = int((eta - eta_min) / (eta_max - eta_min) * N_eta_bins);
            int energy_bin = int((energy - energy_min) / (energy_max - energy_min) * N_energy_bins);
            if (fs_energies[eta_bin][energy_bin] != 0) {
                ratio = energy / fs_energies[eta_bin][energy_bin];
            }

            // Store the energy ratio in the corresponding energy bin and eta bin
            energy_ratios[eta_bin][energy_bin] += ratio;
        }
    }

    // Divide the energy ratios by the number of events to get the average ratio for each energy bin and eta bin
    for (int i = 0; i < N_eta_bins; i++) {
        for (int j = 0; j < N_energy_bins; j++) {
            energy_ratios[i][j] /= n_events;
        }
    }

    // Create a TH2D histogram to store the energy ratios
    TH2D* ratio_hist = new TH2D("ratio_hist", "Energy Ratio;Cluster Energy (GeV);Cluster #eta", N_energy_bins, energy_min, energy_max, N_eta_bins, eta_min, eta_max);

    // Fill the histogram with the energy ratios
    for (int i = 0; i < N_eta_bins; i++) {
        for (int j = 0; j < N_energy_bins; j++) {
            ratio_hist->SetBinContent(j+1, i+1, energy_ratios[i][j]);
        }
    }

    // Create a TCanvas to draw the histogram
    TCanvas* c1 = new TCanvas("c1", "Energy Ratio vs. Cluster Energy and #eta", 800, 600);
    c1->SetRightMargin(0.15);

    // Draw the histogram as a color map
    ratio_hist->Draw("colz");

    // Save the histogram to a file
//    c1->SaveAs("energy_ratio.png");

}



