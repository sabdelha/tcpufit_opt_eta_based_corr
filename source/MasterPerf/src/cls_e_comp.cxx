void cls_e_comp()
{
Int_t singleFile1 = 0;
TString inDir = "./";
inDir="/Users/ska6969/Documents/eta_based/cluster_study/";
TChain * chain_FS = new TChain("trigMetTree");
TChain * chain_LCFS = new TChain("trigMetTree");
//Tchain * chain_both = new TChain("trigMetTree");


std::cout << "Adding files from " << inDir << std::endl;

// define chains for both FS LCFS clusters

chain_FS->Add(inDir+"FS.signal");
TTree * tree_FS = chain_FS;

chain_LCFS->Add(inDir+"LCFS.signal");
TTree * tree_LCFS = chain_LCFS;

//chain_both->Add(inDir+"valid1.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8453_e8455_s3873_s3874_r14239");

//Tree * tree_both = chain_both;

//initialization for FS

std::vector <Float_t> *cls_e =new std::vector <Float_t>();
std::vector <Float_t> *cls_eta =new std::vector <Float_t>();
Float_t mean_e;
Int_t cls_n;

//initialization for LCFS

std::vector <Float_t> *cls_e_lc =new std::vector <Float_t>();
std::vector <Float_t> *cls_eta_lc =new std::vector <Float_t>();
Float_t mean_e_lc;
Int_t cls_n_lc;


//initialization for both

//std::vector <Float_t> *cls_e_lcboth =new std::vector <Float_t>();
//std::vector <Float_t> *cls_eta_lcboth =new std::vector <Float_t>();

//std::vector <Float_t> *cls_e_fsboth =new std::vector <Float_t>(); 
//std::vector <Float_t> *cls_eta_fsboth =new std::vector <Float_t>();


//Float_t mean_e_lcboth;
 //Int_t cls_n_lcboth;

//setting variables to branches
tree_FS->SetBranchAddress("HLT_cluster_e",&cls_e);
tree_FS->SetBranchAddress("HLT_cluster_eta",&cls_eta);
tree_FS->SetBranchAddress("HLT_cluster_energyMean",&mean_e);
tree_FS->SetBranchAddress("HLT_cluster_n",&cls_n);

tree_LCFS->SetBranchAddress("HLT_cluster_e",&cls_e_lc);
tree_LCFS->SetBranchAddress("HLT_cluster_eta",&cls_eta_lc);
tree_LCFS->SetBranchAddress("HLT_cluster_energyMean",&mean_e_lc);
tree_LCFS->SetBranchAddress("HLT_cluster_n",&cls_n_lc);

//tree_both->SetBranchAddress("HLT_cluster_e",&cls_e_lcboth);
//tree_both->SetBranchAddress("HLT_cluster_eta",&cls_eta_lcboth);

//tree_both->SetBranchAddress("cluster_e",&cls_e_fsboth);
//tree_both->SetBranchAddress("cluster_eta",&cls_eta_fsboth);


//defining histograms
TH1F* h_cls_e_FS = new TH1F("h_cls_e_FS", "FS clcuter energy", 100,-2,250);
TH1F* h_cls_e_LCFS = new TH1F("h_cls_e_LCFS", "LCFS cluster enrgy", 100,-2,250);
TH1F* cls_ratio= new TH1F("cls_ratio", "cluster_energies ratio", 100,-2,250);

//TH1F* h_cls_emean = new TH1F("h_cls_emean", "FS clcuter mean energy", 100,-80, 60);
//TH1F* h_cls_emean = new TH1F("h_cls_emean", "LCFS clcuter mean energy", 100,0.0, 300.0);

TH2F* h_eta_energy = new TH2F("h_eta_energy", "FS_clusters' energies Vs clusters' Eta",10, -5, 5,20,0, 200);
TH2F* h_eta_energy_lc = new TH2F("h_eta_energy_lc", "LCFS_clusters' energies Vs clusters' Eta",10, -5, 5,20,0, 200);


TH2F* ratio_et_en= new TH2F("ratio_et_en", "ratio eta energy",10, -5, 5,20,0, 200);




//defining arrays to store energies and Eta
//float_t clsE[1001]={};
//float_t clsEta[1001]={};
//Int_t j=0;

auto c1 = new TCanvas("c1","Profile histogram example",200,10,700,500);
auto E_Vs_eta  = new TProfile("E_Vs_eta","Energy Vs Eta for HLT EM-topoclusters",100,-5,5,-2,70);
auto E_Vs_eta_LCFS  = new TProfile("E_Vs_eta_LCFS","Energy Vs Eta for LCFS clusters",100,-5,5,-2,70);


//looping over events FS
Int_t nevents = (Int_t)tree_FS->GetEntries();
std::cout << "number of events = " << nevents << std::endl;
 for (Int_t i=0;i<nevents;i++) {
 tree_FS->GetEntry(i);
   
   for (Int_t k=0;k<cls_n;k++) {
      // E_Vs_eta->Fill((*cls_eta)[k],(*cls_e)[k]);
//      if(  0 < (*cls_e)[k] &&(*cls_e)[k]<20){
         h_cls_e_FS->Fill((*cls_e)[k]);
       h_eta_energy->Fill((*cls_eta)[k],(*cls_e)[k]);
//debuging lines

//  std::cout << "size of the vector = " << cls_e->size() << std::endl;
//  std::cout << "number of clusters = " << cls_n << std::endl;
//    std::cout << "clse = " << (*cls_e)[500] << std::endl;

 //std::cout << "mean = " << mean_e<< std::endl;
    
   //h_cls_emean->Fill(mean_e);
  // h_cls_e->Fill((*cls_e)[0]);
  // h_cls_eta->Fill((*cls_eta)[0]);
  // clsE[j]=((*cls_e)[1]);
  // clsEta[j]=((*cls_eta)[1]);
   // j+=1;
 
  //E_Vs_eta->Fill((*cls_eta)[k],(*cls_e)[k],1);
 //E_Vs_eta_LCFS->Fill((*cls_eta_lc)[k],(*cls_e_lc)[k],1);   
  //std::cout << "clsE = " << clsE[i] << std::endl;
   
//h_eta_energy->Fill((*cls_eta)[k],(*cls_e)[k]);
 // }
}
}

Int_t nevents1 = (Int_t)tree_LCFS->GetEntries();
std::cout << "number of events = " << nevents1 << std::endl;
 for (Int_t i=0;i<nevents1;i++) {
 tree_LCFS->GetEntry(i);
   for (Int_t m=0;m<cls_n_lc;m++) {
    //   E_Vs_eta_LCFS->Fill((*cls_eta_lc)[m],(*cls_e_lc)[m]);
       h_eta_energy_lc->Fill((*cls_eta_lc)[m],(*cls_e_lc)[m]);
  //  if((*cls_eta_lc)[m]>3.5 && ((*cls_eta_lc)[m])<4.5 ){
//     if(  0 < (*cls_e_lc)[m] &&(*cls_e_lc)[m]<20){
        h_cls_e_LCFS->Fill((*cls_e_lc)[m]);

  //h_eta_energy_lc->Fill((*cls_eta)[m],(*cls_e)[m]);
  //E_Vs_eta_LCFS->Fill((*cls_eta_lc)[m],(*cls_e_lc)[m],1);

}
}

//h_eta_energy_lc->Draw("LEGO1");
//h_cls_emean->Draw();
//h_cls_e->Draw();
/*
//E_Vs_eta_LCFS->Draw();
E_Vs_eta->SetLineColor(kRed);
//E_Vs_eta->Draw();
E_Vs_eta->Draw("HIST L SAME");
E_Vs_eta_LCFS->Draw("HIST L SAME");





E_Vs_eta->GetYaxis()->SetTitle("Cluster Energy [GeV]");
E_Vs_eta->GetYaxis()->SetTitleOffset(1.3); 
E_Vs_eta->GetYaxis()->CenterTitle(true);

E_Vs_eta->GetXaxis()->SetTitle("Cluster Eta");
E_Vs_eta->GetYaxis()->SetTitleOffset(1.3); 
E_Vs_eta->GetYaxis()->CenterTitle(true);


//E_Vs_eta_LCFS->Draw();
//E_Vs_eta->SetLineColor(kRed);
//E_Vs_eta->Draw("same");
//E_Vs_eta_LCFS->Draw("same");
h_eta_energy->Draw("colz");

auto legend = new TLegend(0.1,0.8,0.3,0.9);
legend->SetHeader("FS Vs LCFS clusters E_Vs_eta ","C"); // option "C" allows to center the header
legend->AddEntry(E_Vs_eta_LCFS,"LCFS clusters E_Vs_eta","l");
legend->AddEntry(E_Vs_eta,"FS clusters E_Vs_eta","l");
legend->Draw();
*/

 //  h_cls_e_FS->GetXaxis()->SetTitle("FS ClsEta");
 //  h_cls_e_FS->GetYaxis()->SetTitle("FS ClsEnergy");
 //  h_cls_e_LCFS->GetXaxis()->SetTitle("LCFS ClsEta");
 //  h_cls_e_LCFS->GetYaxis()->SetTitle("LCFS ClsEnergy");

//   t2->Draw("FS_eta:FS_En >> h_eta_energy","","goff");
 //  t2->Draw("LCFS_eta:LCFS_En >> h_eta_energy_lc","","goff");
/*
   TCanvas *c2 = new TCanvas("c2","c2",800,600) ;
   c2->Divide(2,2);
   c2->cd(1);
   h_cls_e_FS->Draw();
   c2->cd(2);
   h_cls_e_LCFS->Draw();

   cls_ratio = (TH1F*)h_cls_e_LCFS->Clone();
   cls_ratio->GetXaxis()->SetTitle(" ");
   cls_ratio->GetYaxis()->SetTitle(" ");
   cls_ratio->SetTitle("h1/h2");
   cls_ratio->Divide(h_cls_e_FS);
   c2->cd(3);
   cls_ratio->Draw();
*/

//cls_ratio->Divide(h_cls_e_LCFS,h_cls_e_FS ,1.0,1.0,"B");
ratio_et_en->Divide(h_eta_energy_lc,h_eta_energy);
ratio_et_en->Draw("colz");
//h_eta_energy->Draw("colz");
//ratio_et_en->Draw();
//h_cls_e_FS->Draw();
//ratio_et_en->Draw("colz");



//h_cls_e_LCFS->Draw();
//h_cls_e_FS->Draw();
cls_ratio->Divide(h_cls_e_LCFS,h_cls_e_FS);
//cls_ratio->Draw();



//TH1D *h1 = E_Vs_eta_LCFS->ProjectionX();
//TH1D *h2 = E_Vs_eta->ProjectionX();
//h2->Divide(h1);

//h2->Draw();



}
