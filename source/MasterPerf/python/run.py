"""Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

The main run script"""


import argparse
import functools
from typing import List


# Run argparse *before* importing any ATLAS code to avoid slow imports on --help
parser = argparse.ArgumentParser()
parser.add_argument(
    "input_files", type=str, nargs="*", help="The names of the input files"
)
parser.add_argument(
    "-i",
    "--input",
    default=[],
    type=functools.partial(str.split, sep=","),
    help="Names of further input files separated by a comma",
)
parser.add_argument(
    "-t",
    "--trigger-selection",
    type=str,
    nargs="*",
    default=["default"],
    help="Names of triggers to be required in an OR. A few special names are also "
    "allowed which can be interpreted to predefined lists: 'muon' will choose a "
    "pre-selected list of valid (R3) muon triggers, 'none' will apply no trigger "
    "selection, 'default' will pick a default selection for that data type (L1_XE50 "
    "for EB data, L1_XE50 || muon for other data/MC)",
)
parser.add_argument(
    "-o",
    "--output",
    type=str,
    default="tree.root",
    help="The name of the ROOT file to create",
)
parser.add_argument(
    "-l",
    "--log-level",
    type=str.upper,
    default="INFO",
    choices=["VERBOSE", "DEBUG", "INFO", "WARNING", "ERROR"],
    help="Global output messaging level",
)
parser.add_argument(
    "-n",
    "--n-events",
    type=int,
    default=-1,
    help="The number of input events to run over. -1 runs over all events",
)
parser.add_argument(
    "-C",
    "--config-only",
    action="store_true",
    help="Only configure the job, do not run"
)
parser.add_argument(
    "--is-eb",
    action="store_true",
    help="The input is an EB run and the weights should be calculated",
)
args = parser.parse_args()

from AthenaCommon import Constants
from AthenaConfiguration.AthConfigFlags import AthConfigFlags
from AthenaConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.Enums import ProductionStep
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
from MasterPerf.AnaConfig import AnaCfg
from METReconstruction.METTruth_Cfg import METTruth_Cfg


def trigger_list(flags: AthConfigFlags, trig: str) -> List[str]:
    """Build a trigger list from a list of triggers + nick names"""
    if trig.lower() == "muon":
        return [
            "HLT_mu24_ivarmedium_L1MU14FCH",
            "HLT_mu26_ivarmedium_L1MU14FCH",
            "HLT_mu28_ivarmedium_L1MU14FCH",
            "HLT_mu24_ivarmedium_L1MU18VFCH",
            "HLT_mu26_ivarmedium_L1MU18VFCH",
            "HLT_mu28_ivarmedium_L1MU18VFCH",
        ]
    elif trig.lower() == "default":
        triggers = ["L1_XE50"]
        if flags.Input.isMC or not args.is_eb:
            triggers += trigger_list(flags, "muon")
        return triggers
    else:
        return [trig]


ConfigFlags.Input.Files = args.input_files + args.input
ConfigFlags.Exec.OutputLevel = getattr(Constants, args.log_level)
# Tell the code that we are running after Reconstruction
ConfigFlags.Common.ProductionStep = ProductionStep.Derivation
ConfigFlags.lock()
ConfigFlags.initAll()
ConfigFlags.dump()

# Set up the CA
acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolReadCfg(ConfigFlags))
# Stop it printing so much
acc.getService("AthenaEventLoopMgr").EventPrintoutInterval = 1000
acc.addService(
    CompFactory.THistSvc(
        "THistSvc", Output=[f"TREE DATAFILE='{args.output}' OPT='RECREATE'"]
    )
)

# Create the analysis sequence
seq_name = "AnalysisSequence"
acc.addSequence(CompFactory.AthSequencer(seq_name, Sequential=True, StopOverride=False))

# Get the TDT
tdt = acc.getPrimaryAndMerge(TrigDecisionToolCfg(ConfigFlags))

triggers: List[str] = []
for trig in args.trigger_selection:
    if trig.lower() == "none":
        triggers = []
        break
    triggers += trigger_list(ConfigFlags, trig)

if triggers:
    acc.addEventAlgo(
        CompFactory.TrigFilterAlg(TrigDecisionTool=tdt, Triggers=triggers),
        sequenceName=seq_name,
    )

# Run the analysis sequence
acc.merge(AnaCfg(ConfigFlags, jet_type="AntiKt4EMPFlow", sequence_name=seq_name), sequenceName=seq_name)

output_mets = [
    "HLT_MET_tcpufit",
    "HLT_MET_cell",
    "HLT_MET_trkmht",
    "HLT_MET_cvfpufit",
    "HLT_MET_pfopufit",
    "HLT_MET_mhtpufit_em",
    "HLT_MET_mhtpufit_pf",
    "HLT_MET_pfsum",
    "HLT_MET_pfsum_vssk",
    "HLT_MET_pfsum_cssk",
    "HLT_MET_tcpufit_LCFS",
    "HLT_MET_tcpufit_FS",
    "HLT_MET_tcpufit_scaled_FS",   


]

output_triggers = list(
    set(
        triggers
        + [
            "HLT_xe65_cell_xe90_pfopufit_L1XE50",
            "HLT_xe65_cell_xe100_pfopufit_L1XE50",
            "HLT_xe75_cell_xe100_pfopufit_L1XE50",
            "HLT_xe75_cell_xe65_tcpufit_xe90_trkmht_L1XE50",
            "HLT_xe60_cell_xe95_pfsum_cssk_L1XE50",
            "HLT_xe55_cell_xe70_tcpufit_xe90_pfsum_vssk_L1XE50",
            "HLT_xe65_cell_xe105_mhtpufit_em_L1XE50",
            "HLT_xe65_cell_xe100_mhtpufit_pf_L1XE50",
            "HLT_xe55_cell_xe70_tcpufit_xe95_pfsum_cssk_L1XE50",
            "HLT_xe65_cell_xe95_pfsum_vssk_L1XE50",
        ]
    )
)

# If it's MC, create the truth MET
if ConfigFlags.Input.isMC:
    acc.merge(METTruth_Cfg(ConfigFlags), sequenceName=seq_name)
#run tcpufitFex algorithm on TopoCaloClustersLCFS:

tcpufit_LCFS= CompFactory.HLT.MET.TCPufitFex("HLT_MET_tcpufit_LCFS",
METContainerKey="HLT_MET_tcpufit_LCFS",
ClusterName="HLT_TopoCaloClustersLCFS",

)


#run tcpufitFex algorithm on EM TopoCaloClustersFS:

tcpufit_FS = CompFactory.HLT.MET.TCPufitFex("HLT_MET_tcpufit_FS",
METContainerKey="HLT_MET_tcpufit_FS",
ClusterName="HLT_TopoCaloClustersFS",

)
# run newcluster algorithm on EM TopoCaloClustersFS to produce scaled copy of the clusters:
newcluster_alg = CompFactory.newcluster(
InputCluster="HLT_TopoCaloClustersFS",

)

# run tcpufitFex algorithm on EM scaled TopoCaloClustersFS (the output of newclusteralgorithm):

tcpufit_FScaled = CompFactory.HLT.MET.TCPufitFex("HLT_MET_tcpufit_scaled_FS",
METContainerKey="HLT_MET_tcpufit_scaled_FS",
ClusterName="outputClusters",

)





# Run the ntuple algorithm
ntuple_alg = CompFactory.NTupleAlg(
    IsEB=args.is_eb,
    IsMC=ConfigFlags.Input.isMC,
    TrigDecisionTool=tdt,
    TriggerMETs=output_mets,
    Triggers=output_triggers,
    RecoMET="AnalysisMET",
    Muons="AnalysisMuons",
    RootStreamName="TREE",
)
if args.is_eb:
    ntuple_alg.EBWeightsTool = CompFactory.EnhancedBiasWeighter(
        RunNumber=ConfigFlags.Input.RunNumber[0],
        UseBunchCrossingData=False,
    )
if ConfigFlags.Input.isMC:
    ntuple_alg.TruthMET = "MET_Truth"

acc.addEventAlgo(tcpufit_LCFS, sequenceName=seq_name)
acc.addEventAlgo(tcpufit_FS, sequenceName=seq_name)
acc.addEventAlgo(newcluster_alg, sequenceName=seq_name)
acc.addEventAlgo(tcpufit_FScaled, sequenceName=seq_name)
acc.addEventAlgo(ntuple_alg, sequenceName=seq_name)

acc.printConfig(withDetails=True, summariseProps=True)
if not args.config_only:
    acc.run(args.n_events)
