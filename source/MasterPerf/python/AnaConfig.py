"""Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

Module containing the necessary analysis configuration"""

import logging
from typing import Any, Callable, Dict, Union
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.AthConfigFlags import AthConfigFlags
from AthenaConfiguration.Enums import LHCPeriod
from AthenaCommon.SystemOfUnits import GeV
from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import (
    makeElectronAnalysisSequence,
)
from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
from JetAnalysisAlgorithms.JetAnalysisSequence import makeJetAnalysisSequence
from MetAnalysisAlgorithms.MetAnalysisSequence import makeMetAnalysisSequence
from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence
from IsolationAlgs.DerivationTrackIsoConfig import DerivationTrackIsoCfg
from eflowRec.PFCfg import PFGlobalFlowElementLinkingCfg
from JetRecConfig.JetRecConfig import JetRecCfg
from JetRecConfig import StandardSmallRJets
from JetRecConfig.JetConfigFlags import jetInternalFlags
from METReconstruction.METAssociatorCfg import METAssociatorCfg
import ROOT

log = logging.getLogger(__name__)


def data_type(flags: AthConfigFlags) -> str:
    """Get the CP algs data type string from the config flags"""
    # TODO: Right now this does not handle AFII
    return "mc" if flags.Input.isMC else "data"


def ana_sequence_ca(
    flags: AthConfigFlags,
    seq: Callable,
    *args,
    sequence_name: str,
    input_name: Union[str, Dict[str, str]],
    output_name: Union[str, Dict[str, str]],
    **kwargs: Any,
) -> ComponentAccumulator:
    """Create a ComponentAccumulator from a CP alg configuration function

    The data type (and only the data type) will be deduced from the flags

    Parameters
    ----------
    flags : AthConfigFlags
        The configuration flags
    seq : Callable
        The analysis configuration function
    sequence_name : str
        The name of the sequence in which to put the CP algs
    input_name : Union[str, Dict[str, str]]
        The input name to the configure method
    output_name : Union[str, Dict[str, str]]
        The output name from the configure method
    kwargs : Any
        Keyword arguments are forwarded to the function
    """
    acc = ComponentAccumulator()
    sequence = seq(data_type(flags), *args, **kwargs)
    sequence.configure(inputName=input_name, outputName=output_name)
    acc.addSequence(CompFactory.AthSequencer(sequence_name, Sequential=True, StopOverride=False))
    for algo in sequence.getGaudiConfig2Components():
        acc.addEventAlgo(algo, sequenceName=sequence_name)
    return acc


def AnaCfg(
    flags: AthConfigFlags,
    jet_type: str = "AntiKt4EMPFlow",
    sequence_name="AnalysisSequence",
) -> ComponentAccumulator:
    """Create the analysis configuration"""
    acc = ComponentAccumulator()
    # Even running on MC we don't want any systematics
    acc.addService(CompFactory.CP.SystematicsSvc("SystematicsSvc", systematicsList=[]))
    # Create the sequence
    acc.addSequence(
        CompFactory.AthSequencer(sequence_name, Sequential=True, StopOverride=False)
    )

    # TODO: Find a way to determine these from the ConfigFlags
    is_aod = True
    is_run3 = flags.GeoModel.Run >= LHCPeriod.Run3
    if is_aod:
        # Update the isolation variables
        acc.merge(DerivationTrackIsoCfg(
            flags, object_types=("Electrons", "Muons")), sequenceName=sequence_name
        )
        # Run the jet building and MET association
        jetInternalFlags.isRecoJob = True
        try:
            jetdef = getattr(StandardSmallRJets, jet_type)
        except AttributeError:
            log.error(f"Unknown jet type '{jet_type}' requested!")
            raise
        acc.merge(JetRecCfg(flags, jetdef=jetdef), sequenceName=sequence_name)
        acc.merge(PFGlobalFlowElementLinkingCfg(flags), sequenceName=sequence_name)
        acc.merge(METAssociatorCfg(flags, jetType=jet_type), sequenceName=sequence_name)

    # Right now we haven't got PRW set up for run 3 MC samples
    if flags.Input.isMC and not is_run3:
        prw_acc = ana_sequence_ca(
            flags,
            makePileupAnalysisSequence,
            sequence_name=sequence_name,
            input_name="EventInfo",
            output_name="EventInfo",
        )
        acc.merge(prw_acc)

    # Set up our sequences
    ele_acc = ana_sequence_ca(
        flags,
        makeElectronAnalysisSequence,
        sequence_name=sequence_name,
        input_name="Electrons",
        output_name="AnalysisElectrons",
        workingPoint="LooseLHElectron.Loose_VarRad",
        recomputeLikelihood=is_aod,
        shallowViewOutput=True,
        ptSelectionOutput=True,
    )
    if flags.Input.isMC and is_run3:
        # If we're running on MC but not using PRW we won't have the RRN
        # For calibration, set it to a 2016 one
        ele_acc.getEventAlgo(
            "ElectronCalibrationAndSmearingAlg"
        ).calibrationAndSmearingTool.randomRunNumber = ROOT.EgammaCalibPeriodRunNumbersExample.run_2016
        ele_acc.getEventAlgo(
            "ElectronEfficiencyCorrectionAlg"
        ).efficiencyCorrectionTool.UseRandomRunNumber = False
    ele_acc.getEventAlgo("ElectronPtCutAlg").selectionTool.minPt = 10 * GeV
    acc.merge(ele_acc)

    muon_acc = ana_sequence_ca(
        flags,
        makeMuonAnalysisSequence,
        sequence_name=sequence_name,
        input_name="Muons",
        output_name="AnalysisMuons",
        workingPoint="Medium.Iso",
        shallowViewOutput=True,
        ptSelectionOutput=True,
    )
    if flags.Input.isMC and is_run3:
        muon_acc.getEventAlgo(
            "MuonCalibrationAndSmearingAlg"
        ).calibrationAndSmearingTool.useRandomRunNumber = False
        # No way to turn off RRN dependence in the muon efficiency scale factors
        muon_acc.dropEventAlgo("MuonEfficiencyScaleFactorAlg", sequence=sequence_name)
    muon_acc.getEventAlgo("MuonPtCutAlg").selectionTool.minPt = 10 * GeV
    muon_acc.getEventAlgo("MuonSelectionAlg").selectionTool.IsRun3Geo = is_run3
    acc.merge(muon_acc)

    jet_acc = ana_sequence_ca(
        flags,
        makeJetAnalysisSequence,
        f"{jet_type}Jets",
        sequence_name=sequence_name,
        input_name=f"{jet_type}Jets",
        output_name="AnalysisJets",
        shallowViewOutput=False,
        runNNJvtUpdate=True,
        runJvtEfficiency=False,
    )
    nnjvt_alg = jet_acc.getEventAlgo("NNJvtUpdateAlg")
    # Not entirely sure why this part is necessary
    nnjvt_alg.decorator.JetContainer = nnjvt_alg.jetsOut
    acc.merge(jet_acc)

    met_acc = ana_sequence_ca(
        flags,
        makeMetAnalysisSequence,
        jet_type,
        sequence_name=sequence_name,
        input_name={
            "jets": "AnalysisJets",
            "electrons": "AnalysisElectrons",
            "muons": "AnalysisMuons",
        },
        output_name="AnalysisMET",
    )
    met_acc.getEventAlgo("MetBuilderAlg").finalKey = "FinalTrk"
    met_acc.getEventAlgo("MetSignificanceAlg").totalMETName = "FinalTrk"
    acc.merge(met_acc)
    return acc
