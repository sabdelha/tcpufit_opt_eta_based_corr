#include <AsgMessaging/MessageCheck.h>
#include <src/newcluster.h>
#include "StoreGate/ReadHandle.h"
#include <xAODCore/AuxContainerBase.h>
#include <xAODJet/JetContainer.h>
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowAuxInfo.h"
#include "xAODCore/ShallowCopy.h"
#include <StoreGate/ReadHandleKey.h>
#include <TFile.h>
#include <TH2F.h>
//#include <iostream>


newcluster :: newcluster (const std::string &name, ISvcLocator *pSvcLocator) : AthReentrantAlgorithm(name, pSvcLocator)
    
{
}

newcluster::~newcluster() {

ratio_et_en = nullptr;
}

StatusCode newcluster :: initialize ()
{
CHECK(m_clusterKey.initialize());
CHECK(m_writeHandle.initialize());

file= new TFile("ratio_histogram.root");
ratio_et_en = (TH2F*)file->Get("ratio_et_en");

//m_systematicsList.addHandle (m_writeHandle);
return StatusCode::SUCCESS;
}


StatusCode newcluster :: execute (const EventContext& context) const 
{
//TFile* file = new TFile("ratio_histogram.root");
//TH2F* ratio_et_en = (TH2F*)file->Get("ratio_et_en");

auto clusters = SG::makeHandle(m_clusterKey,context);
  if (!clusters.isValid())
  {
    ATH_MSG_ERROR("Failed to retrieve " << m_clusterKey);
    return StatusCode::FAILURE;
}
auto newClusterContainers = xAOD::shallowCopyContainer(*clusters);
//auto newauxClusterContainers = xAOD::ShallowAuxContainer(*clusters);

auto newcont = std::unique_ptr<xAOD::CaloClusterContainer>(newClusterContainers.first);
auto newcontAux = std::unique_ptr<xAOD::ShallowAuxContainer>(newClusterContainers.second);

newcont->setStore (newcontAux.get());


for (auto  iclus : *(newClusterContainers.first))
{
      float_t newpt=0;

      //float_t eta= abs(iclus->eta());
     int bin = ratio_et_en->FindBin(iclus->eta(),iclus->e());     
     newpt = iclus->e()*(ratio_et_en->GetBinContent(bin));
      

   iclus->setE(newpt);


}

auto outputClusters = SG::makeHandle(m_writeHandle,context);

CHECK(outputClusters.record(std::move(newcont), std::move(newcontAux)));




return StatusCode::SUCCESS;
}



DECLARE_COMPONENT(newcluster)



//StatusCo
//{




//return StatusCode::SUCCESS;
//}



