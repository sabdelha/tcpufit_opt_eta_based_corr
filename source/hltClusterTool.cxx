#include <AsgMessaging/MessageCheck.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODCore/ShallowAuxContainer.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODCaloEvent/CaloClusterContainer.h>


#include <trigAnalysis/hltClusterTool.h>

#include <vector> 


hltClusterTool :: hltClusterTool (const std::string& name)
  : AsgTool (name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.

  declareProperty( "BranchPrefix", m_branchPrefix = "HLT_FS", "Prefix to be prepended to all branch names." );
  declareProperty("clusterType", m_clusterType = 0, "0=HLT, 1=Offline");
  declareProperty( "hltClusterContainer", m_hltClusterContainer = "HLT_TopoCaloClustersFS","hlt cluster container name" );
}

hltClusterTool :: ~hltClusterTool ()
{
}

StatusCode hltClusterTool::initialize()
{
  ANA_MSG_DEBUG( "Initialising hltClusterTool " << name() );

  return StatusCode::SUCCESS;
}

StatusCode hltClusterTool::finalize()
{
  ANA_MSG_DEBUG( "Finalising hltClusterTool " << name() );

  return StatusCode::SUCCESS;
}

StatusCode hltClusterTool::addBranches(TTree* tree)
{
  if (!tree) {
    ANA_MSG_ERROR( "Invalid tree pointer supplied to hltClusterTool::addBranches!" );
    return StatusCode::FAILURE;
  }

  ANA_MSG_DEBUG( "Adding new branches to tree " << tree->GetName() );
  ANA_CHECK( addBranch(tree, "cluster_n", &m_cluster_n) );
  ANA_CHECK( addBranch(tree, "cluster_e", &m_cluster_e) );
  ANA_CHECK( addBranch(tree, "cluster_eta", &m_cluster_eta) );
  ANA_CHECK( addBranch(tree, "cluster_moment", &m_cluster_moment) );
  ANA_CHECK( addBranch(tree, "cluster_phi", &m_cluster_phi) );
  ANA_CHECK( addBranch(tree, "cluster_m", &m_cluster_m) );
  ANA_CHECK( addBranch(tree, "cluster_time", &m_cluster_time) );
  ANA_CHECK( addBranch(tree, "cluster_energySum", &m_energySum) );
  ANA_CHECK( addBranch(tree, "cluster_energyMean", &m_energyMean) );
  ANA_CHECK( addBranch(tree, "cluster_energyVariance", &m_energyVariance) );
  ANA_CHECK( addBranch(tree, "cluster_energySigma", &m_energySigma) );

  return StatusCode::SUCCESS;
}

StatusCode hltClusterTool::updateValues()
{
  // initialize member variables
  m_cluster_n = 0;
  m_cluster_e_ptr->clear();
  m_cluster_eta_ptr->clear();
  m_cluster_moment_ptr->clear();

  m_cluster_phi_ptr->clear();
  m_cluster_m_ptr->clear();
  m_cluster_time_ptr->clear();
  m_energySum = 0;
  m_energyMean = 0;
  m_energyVariance = 0;
  m_energySigma = 0;

  // retrieve containers
  // hlt cluster object from the event store
  const xAOD::CaloClusterContainer *hltClusters = nullptr;
  ANA_CHECK (evtStore()->retrieve (hltClusters, m_hltClusterContainer));

  // fill the branches of the tree
  // loop over hlt clusters
  float varSum = 0;
  if (hltClusters) {
    m_cluster_n = hltClusters->size();
    ANA_MSG_DEBUG( "Number of HLT clusters = " << m_cluster_n );
    for (auto hltCluster : *hltClusters) {
      float cluster_e = 0;
      float cluster_eta = 0;
      float cluster_moment=0;
      float cluster_phi = 0;
      float cluster_m = 0;
      if (m_clusterType) { //offline clusters
        cluster_e = hltCluster->pt(xAOD::CaloCluster::CALIBRATED)*0.001;
        cluster_eta = hltCluster->eta(xAOD::CaloCluster::CALIBRATED);
        cluster_moment = hltCluster->getMomentValue(xAOD::CaloCluster::ENG_FRAC_EM);
        cluster_phi = hltCluster->phi(xAOD::CaloCluster::CALIBRATED);
        cluster_m = hltCluster->m(xAOD::CaloCluster::CALIBRATED)*0.001;
      } else { //HLT clusters
        cluster_e = hltCluster->calE()*0.001;
        cluster_eta = hltCluster->calEta();
        cluster_phi = hltCluster->calPhi();
        cluster_m = hltCluster->calM()*0.001;
       cluster_moment=getMomentValue(); 
    }
    //  cluster_moment=hltCluster->getMomentValue(xAOD::CaloCluster::ENG_FRAC_EM);
      m_cluster_e_ptr->push_back(cluster_e);
      m_cluster_eta_ptr->push_back(cluster_eta);
      m_cluster_moment_ptr->push_back(cluster_moment);
      m_cluster_phi_ptr->push_back(cluster_phi);
      m_cluster_m_ptr->push_back(cluster_m);
      m_cluster_time_ptr->push_back(hltCluster->time());
      m_energySum += cluster_e;
    }
    if (m_cluster_n > 0) m_energyMean = m_energySum / m_cluster_n;

    // loop over clsuters again to calculate variance
    for (auto hltCluster : *hltClusters) {
      float cluster_e = 0;
      if (m_clusterType) { //offline clusters
        cluster_e = hltCluster->pt(xAOD::CaloCluster::CALIBRATED)*0.001;
      } else { //HLT clusters
        cluster_e = hltCluster->calE()*0.001;
      }
      varSum += (cluster_e - m_energyMean)*(cluster_e - m_energyMean);
    }
    if (m_cluster_n > 0) m_energyVariance = varSum / m_cluster_n;
    m_energySigma = std::sqrt(m_energyVariance);
  }
  

  ANA_MSG_DEBUG("Finished updateValues");
  return StatusCode::SUCCESS;
}



