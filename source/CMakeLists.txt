cmake_minimum_required( VERSION 3.12 FATAL_ERROR )

project( MasterPerfStudies VERSION 1.0.0 )

find_package( AtlasCMake QUIET )

set( ATLAS_PROJECT Athena )

find_package( ${ATLAS_PROJECT} REQUIRED )

project( MasterPerfStudies VERSION ${${ATLAS_PROJECT}_VERSION} LANGUAGES C CXX )

atlas_ctest_setup()

set( CMAKE_EXPORT_COMPILE_COMMANDS TRUE CACHE BOOL
  "Create compile_commands.json" FORCE)

atlas_project( USE ${ATLAS_PROJECT} ${${ATLAS_PROJECT}_VERSION} )

lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
     DESTINATION . )

atlas_cpack_setup()
