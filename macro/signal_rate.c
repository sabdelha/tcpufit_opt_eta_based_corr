void signal_rate()
{

//define the input directory and files:

Int_t singleFile = 0;
TString inDir = "./";

inDir="/Users/ska6969/Documents/eta_based/";

 TChain * chain = new TChain("METTree");

 std::cout << "Adding files from " << inDir << std::endl;

 chain->Add(inDir+"tree_signal.root");

 TTree * tree = chain;

 float_t offline_et;
 float_t tcPufit_LCFS_et;
 float_t tcPufit_FS_et;
 float_t tcPufit_FS_scaled_et;
 float_t L1_et;


 tree->SetBranchAddress("tcpufit_scaled_FS.met",&tcPufit_FS_scaled_et);
 tree->SetBranchAddress("tcpufit_FS.met",&tcPufit_FS_et);
 tree->SetBranchAddress("tcpufit_LCFS.met",&tcPufit_LCFS_et);
 tree->SetBranchAddress("L1.met",&L1_et);
 tree->SetBranchAddress("Reco.met",&offline_et);
 //TH1F* h_tcPufit_MET = new TH1F("h_tcPufit_MET5", "; Tcpufit MET [GeV];(tcpufit)",100,0,400);



float_t j_fs[401]={0};
float_t j_lcfs[401]={0};
float_t j_scfs[401]={0};

Int_t nevents = (Int_t)tree->GetEntries();



 std::cout << "number of events = " << nevents << std::endl;


   for (Int_t i=0;i<nevents;i++)  {
       tree->GetEntry(i);
      //h_tcPufit_MET->Fill(tcPufit_et*0.001);
     if (L1_et *0.001>50 && offline_et *0.001>175){
      
       for (int l=0;l<400;l++){
          if (tcPufit_FS_et*0.001>l ){
             j_fs[l]=j_fs[l]+1;


              }
           if (tcPufit_LCFS_et*0.001>l ){
               j_lcfs[l]=j_lcfs[l]+1;
        }
          if (tcPufit_FS_scaled_et*0.001>l ){
              j_scfs[l]=j_scfs[l]+1;




}


}
}
   }
//&& offline_et *0.001>175






 TChain * chain1 = new TChain("METTree");

 //std::cout << "Adding files from " << inDir << std::endl;

 chain1->Add(inDir+"tree_bg.root");

 TTree * tree1 = chain1;

 float_t offline_et1;
 float_t tcPufit_LCFS_et1;
 float_t tcPufit_FS_et1;
 float_t tcPufit_FS_scaled_et1;
 float_t L1_et1;
float_t ebw;

 tree1->SetBranchAddress("tcpufit_scaled_FS.met",&tcPufit_FS_scaled_et1);
 tree1->SetBranchAddress("tcpufit_FS.met",&tcPufit_FS_et1);
 tree1->SetBranchAddress("tcpufit_LCFS.met",&tcPufit_LCFS_et1);
 tree1->SetBranchAddress("L1.met",&L1_et1);
 tree1->SetBranchAddress("Reco.met",&offline_et1);
 tree1->SetBranchAddress("EBWeight",&ebw);

 //TH1F* h_tcPufit_MET = new TH1F("h_tcPufit_MET5", "; Tcpufit MET [GeV];(tcpufit)",100,0,400);



float_t j_fs1[401]={0};
float_t j_lcfs1[401]={0};
float_t j_scfs1[401]={0};

Int_t nevents1 = (Int_t)tree1->GetEntries();



 std::cout << "number of events1 = " << nevents1 << std::endl;


   for (Int_t i=0;i<nevents1;i++)  {
       tree1->GetEntry(i);
      //h_tcPufit_MET->Fill(tcPufit_et1*0.001);
      if (L1_et1 *0.001>50){

       for (int l=0;l<400;l++){
          if (tcPufit_FS_et1*0.001>l ){
             j_fs1[l]=j_fs1[l]+ebw;


              }
           if (tcPufit_LCFS_et1*0.001>l ){
               j_lcfs1[l]=j_lcfs1[l]+ebw;
        }
          if (tcPufit_FS_scaled_et1*0.001>l ){
              j_scfs1[l]=j_scfs1[l]+ebw;




}


}
}

}

int n=400;
Double_t x[n];
Double_t signalfs[n];
Double_t signallcfs[n];
Double_t signalscfs[n];

Double_t bgfs[n];
Double_t bglcfs[n];
Double_t bgscfs[n];



  for (int i=0; i<n; i++) {
      signalfs[i] = (j_fs[i])/(j_fs[0]);
      signallcfs[i] = (j_lcfs[i])/(j_lcfs[0]);
      signalscfs[i] = (j_scfs[i])/(j_scfs[0]);

      bgfs[i] = (j_fs1[i])/(j_fs1[0]);
      bglcfs[i] = (j_lcfs1[i])/(j_lcfs1[0]);
      bgscfs[i] = (j_scfs1[i])/(j_scfs1[0]);



    x[i]=i;


    }

//auto gr_fs = new TGraph (n, x, signalfs);
//auto gr_fs = new TGraph (n, x, bgfs);
auto gr_fs = new TGraph (n, signalfs, bgfs);
gStyle->SetOptTitle(kFALSE);
gStyle->SetOptStat(0);
//gr_fs->SetTitle("Fraction of signal events Vs tcpufit MET (GeV) using EM_topoclusters");
//gr_fs->SetTitle("Fraction of Background events Vs tcpufit MET (GeV) using EM_topoclusters");
gr_fs->SetTitle("ROC using EM_topoclusters");

//gr_fs->GetXaxis()->SetTitle("tcpufit met (GeV)");
gr_fs->GetXaxis()->SetTitle("Signal efficiency");

gr_fs->GetXaxis()->SetTitleOffset(1.2);
//gr_fs->GetYaxis()->SetTitle("Fraction of events");
gr_fs->GetYaxis()->SetTitle("Background Acceptance");

gr_fs->GetYaxis()->SetTitleOffset(1.5);
//gr_fs->SetMarkerColor(kBlue);
gr_fs->SetLineColor(4);
gr_fs->SetLineWidth(4);
//gr_fs->SetMarkerColor(4);
//gr_fs->SetMarkerSize(0.5);
//gr_fs->SetMarkerStyle(21);
gr_fs->GetYaxis()->SetRangeUser(0.00001,1);
//gr3->SetNpx(1000);
gr_fs->Draw();
//gPad->SetLogy();
//gPad->BuildLegend();

//auto gr_lcfs = new TGraph (n, x, signallcfs);
//auto gr_lcfs = new TGraph (n, x, bglcfs);
auto gr_lcfs = new TGraph (n, signallcfs, bglcfs);

gStyle->SetOptTitle(kFALSE);
gStyle->SetOptStat(0);
//gr_lcfs->SetTitle("Fraction of signal events Vs tcpufit MET (GeV) using LC clusters");
//gr_lcfs->SetTitle("Fraction of Background events Vs tcpufit MET (GeV) using LC clusters");
gr_lcfs->SetTitle("ROC using LC clusters");

//gr_lcfs->GetXaxis()->SetTitle("tcpufit met (GeV)");

gr_lcfs->GetXaxis()->SetTitle("Signal efficiency");

gr_lcfs->GetXaxis()->SetTitleOffset(1.2);
//gr_lcfs->GetYaxis()->SetTitle("Fraction of events");
gr_lcfs->GetYaxis()->SetTitle("Background Acceptance");

gr_lcfs->GetYaxis()->SetTitleOffset(1.5);
//gr_lcfs->SetMarkerColor(kBlue);
gr_lcfs->SetLineColor(2);
gr_lcfs->SetLineWidth(4);
//gr_lcfs->SetMarkerColor(4);
//gr_lcfs->SetMarkerSize(0.5);
//gr_lcfs->SetMarkerStyle(21);
gr_lcfs->GetYaxis()->SetRangeUser(0.00001,1);
gr_lcfs->Draw();

//auto gr_scfs = new TGraph (n, x, signalscfs);
//auto gr_scfs = new TGraph (n, x, bgscfs);
auto gr_scfs = new TGraph (n, signalscfs, bgscfs);

gStyle->SetOptTitle(kFALSE);
gStyle->SetOptStat(0);
//gr_scfs->SetTitle("Fraction of signal events Vs tcpufit MET (GeV) using corrected EM_topoclusters");
//gr_scfs->SetTitle("Fraction of Background events Vs tcpufit MET (GeV) using corrected EM_topoclusters");
gr_scfs->SetTitle("ROC using corrected EM_topoclusters");

//gr_scfs->GetXaxis()->SetTitle("tcpufit met (GeV)");
gr_scfs->GetXaxis()->SetTitle("Signal efficiency");

gr_scfs->GetXaxis()->SetTitleOffset(1.2);
//gr_scfs->GetYaxis()->SetTitle("Fraction of events");
gr_scfs->GetYaxis()->SetTitle("Background Acceptance");

gr_scfs->GetYaxis()->SetTitleOffset(1.5);
//gr_scfs->SetMarkerColor(kBlue);
gr_scfs->SetLineColor(6);
gr_scfs->SetLineWidth(4);
//gr_scfs->SetMarkerColor(4);
//gr_scfs->SetMarkerSize(0.5);
//gr_scfs->SetMarkerStyle(21);
gr_scfs->GetYaxis()->SetRangeUser(0.00001,1);

gr_scfs->Draw();
gPad->SetLogy();

gPad->BuildLegend();







      }




